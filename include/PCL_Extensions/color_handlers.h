//
// Created by martin on 29.05.21.
//

#ifndef MEDIASSIST3_RELIABILITY_MODIFIER_COLOR_HANDLERS_H
#define MEDIASSIST3_RELIABILITY_MODIFIER_COLOR_HANDLERS_H

#include <pcl/visualization/point_cloud_color_handlers.h>

#include <pcl/common/io.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/principal_curvatures.h>

#include "../Utils.h"


template <class PointT>
class PointCloudColorHandlerCustomAlpha : public pcl::visualization::PointCloudColorHandler<PointT>
{
public:
    using PointCloud = pcl::PointCloud<PointT>;
    using PointCloudPtr = typename PointCloud::Ptr;
    using PointCloudConstPtr = typename PointCloud::ConstPtr;

    PointCloudColorHandlerCustomAlpha () : pcl::visualization::PointCloudColorHandler<PointT>() { }
    PointCloudColorHandlerCustomAlpha (double r, double g, double b, double alpha) : r_(r), g_(g), b_(b), alpha_(alpha)
    {
        capable_ = true;
    }
    PointCloudColorHandlerCustomAlpha (const PointCloudConstPtr &cloud, double r, double g, double b, double alpha) :
            pcl::visualization::PointCloudColorHandler<PointT>(cloud), r_(r), g_(g), b_(b), alpha_(alpha)
    {
        capable_ = true;
    }

    std::string
    getName () const override {
        return ("PointCloudColorHandlerCustomAlpha");
    };

    std::string
    getFieldName () const override {
        return ("custom");
    };

    vtkSmartPointer<vtkDataArray>
    getColor () const override;

private:
    double r_, g_, b_, alpha_;

    using pcl::visualization::PointCloudColorHandler<PointT>::cloud_;
    using pcl::visualization::PointCloudColorHandler<PointT>::capable_;
};

template<class PointT>
vtkSmartPointer<vtkDataArray> PointCloudColorHandlerCustomAlpha<PointT>::getColor() const {
    if (!capable_ || !cloud_)
        return nullptr;

    auto scalars = vtkSmartPointer<vtkUnsignedCharArray>::New ();
    scalars->SetNumberOfComponents (4);

    vtkIdType nr_points = cloud_->size ();
    scalars->SetNumberOfTuples (nr_points);

    // Get a random color
    unsigned char* colors = new unsigned char[nr_points * 4];

    // Color every point
    for (vtkIdType cp = 0; cp < nr_points; ++cp)
    {
        colors[cp * 4 + 0] = static_cast<unsigned char> (r_);
        colors[cp * 4 + 1] = static_cast<unsigned char> (g_);
        colors[cp * 4 + 2] = static_cast<unsigned char> (b_);
        colors[cp * 4 + 3] = static_cast<unsigned char> (alpha_);
    }
    scalars->SetArray (colors, 4 * nr_points, 0, vtkUnsignedCharArray::VTK_DATA_ARRAY_DELETE);
    return scalars;
}


template <class PointT>
class PointCloudColorHandlerCurvature : public pcl::visualization::PointCloudColorHandler<PointT>
{
public:
    using PointCloud = pcl::PointCloud<PointT>;
    using PointCloudPtr = typename PointCloud::Ptr;
    using PointCloudConstPtr = typename PointCloud::ConstPtr;

    PointCloudColorHandlerCurvature () : pcl::visualization::PointCloudColorHandler<PointT>() { }

    PointCloudColorHandlerCurvature (const PointCloudConstPtr &cloud) :
            pcl::visualization::PointCloudColorHandler<PointT>(cloud)
    {
        setInputCloud(cloud);
    }

    std::string
    getName () const override {
        return ("PointCloudColorHandlerReliabilityField");
    };

    std::string
    getFieldName () const override {
        return ("pc1, pc2");
    };

    vtkSmartPointer<vtkDataArray>
    getColor () const override;

    void setInputCloud (const PointCloudConstPtr &cloud) override;

    void setInterpolationBounds(double* low, double* high)
    {
        color_l[0] = low[0];
        color_l[1] = low[1];
        color_l[2] = low[2];

        color_r[0] = high[0];
        color_r[1] = high[1];
        color_r[2] = high[2];
    }

private:
    bool normals_available =  pcl::traits::has_field<PointT, pcl::fields::normal_x>::value &&
                              pcl::traits::has_field<PointT, pcl::fields::normal_y>::value &&
                              pcl::traits::has_field<PointT, pcl::fields::normal_z>::value;
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals {new pcl::PointCloud<pcl::Normal>};

    bool curvature_available = pcl::traits::has_field<PointT, pcl::fields::curvature>::value;
    pcl::PointCloud<pcl::PrincipalCurvatures>::Ptr cloud_curvature {new pcl::PointCloud<pcl::PrincipalCurvatures>};

    double curvature_avg = 0;
    double curvature_min = 1e10;
    double curvature_max = -1e10;
    double color_l[3];
    double color_r[3];

    void lerp(double curvature, double* color) const
    {
        // 1/2 log(x) + 1
        double curvatureNormalized = (curvature - curvature_min) / (curvature_max - curvature_min) + 0.01f;
        curvatureNormalized = (0.5f) * std::log10(curvatureNormalized) + 1;

        color[0] = color_l[0] * (1 - curvatureNormalized) + color_r[0] * curvatureNormalized;
        color[1] = color_l[1] * (1 - curvatureNormalized) + color_r[1] * curvatureNormalized;
        color[2] = color_l[2] * (1 - curvatureNormalized) + color_r[2] * curvatureNormalized;
    }

    double getCurvature(int idx) const
    {
        return std::max(cloud_curvature->points[idx].pc1, cloud_curvature->points[idx].pc2);
    }

    using pcl::visualization::PointCloudColorHandler<PointT>::cloud_;
    using pcl::visualization::PointCloudColorHandler<PointT>::capable_;
    using pcl::visualization::PointCloudColorHandler<PointT>::field_idx_;
    using pcl::visualization::PointCloudColorHandler<PointT>::fields_;
};

template<class PointT>
void PointCloudColorHandlerCurvature<PointT>::setInputCloud(
        const PointCloudColorHandlerCurvature::PointCloudConstPtr &cloud) {
    pcl::visualization::PointCloudColorHandler<PointT>::setInputCloud (cloud);

    if (curvature_available)
    {
        pcl::copyPointCloud<PointT, pcl::PrincipalCurvatures>(*cloud, *cloud_curvature);
    } else {
        if (normals_available)
        {
            pcl::copyPointCloud<PointT, pcl::Normal>(*cloud, *cloud_normals);
        } else {
            pcl::NormalEstimation<PointT, pcl::Normal> ne;
            ne.setInputCloud(cloud);
            typename pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>());
            ne.setSearchMethod(tree);
            ne.setRadiusSearch(0.01);
            ne.compute(*cloud_normals);
        }

        pcl::PrincipalCurvaturesEstimation<PointT, pcl::Normal, pcl::PrincipalCurvatures> pce;
        pce.setInputCloud(cloud);
        pce.setInputNormals(cloud_normals);
        typename pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
        pce.setSearchMethod (tree);
        pce.setRadiusSearch(0.01);
        pce.compute(*cloud_curvature);
    }

    for (int i = 0; i<cloud_curvature->size(); ++i)
    {
        double curvature = getCurvature(i);
        curvature_min = std::min(curvature_min, curvature);
        curvature_max = std::max(curvature_max, curvature);
    }
    curvature_avg = (curvature_max + curvature_min)/2;

    capable_ = true;

}

template<class PointT>
vtkSmartPointer<vtkDataArray> PointCloudColorHandlerCurvature<PointT>::getColor() const {
    if (!capable_ || !cloud_)
        return nullptr;

    auto scalars = vtkSmartPointer<vtkUnsignedCharArray>::New ();
    scalars->SetNumberOfComponents (4);

    vtkIdType nr_points = cloud_->size ();
    scalars->SetNumberOfTuples (nr_points);
    unsigned char* colors = scalars->GetPointer (0);

    // If XYZ present, check if the points are invalid
    int x_idx = -1;
    for (std::size_t d = 0; d < fields_.size (); ++d)
        if (fields_[d].name == "x")
            x_idx = static_cast<int> (d);

    double color_lerp[3];
    if (x_idx != -1)
    {
        int j = 0;
        // Color every point
        for (vtkIdType cp = 0; cp < nr_points; ++cp)
        {
            // Copy the value at the specified field
            if (!std::isfinite ((*cloud_)[cp].x) ||
                !std::isfinite ((*cloud_)[cp].y) ||
                !std::isfinite ((*cloud_)[cp].z))
                continue;
            double curvature = getCurvature(cp);
            lerp(curvature, color_lerp);
            colors[j + 0] = color_lerp[0];
            colors[j + 1] = color_lerp[1];
            colors[j + 2] = color_lerp[2];
            colors[j + 3] = 255;
            j += 4;
        }
    }
    else
    {
        // Color every point
        for (vtkIdType cp = 0; cp < nr_points; ++cp)
        {
            double curvature = getCurvature(cp);
            lerp(curvature, color_lerp);
            int j = static_cast<int> (cp) * 4;
            colors[j + 0] = color_lerp[0];
            colors[j + 1] = color_lerp[1];
            colors[j + 2] = color_lerp[2];
            colors[j + 3] = 255;
        }
    }
    return scalars;
}

#endif //MEDIASSIST3_RELIABILITY_MODIFIER_COLOR_HANDLERS_H
