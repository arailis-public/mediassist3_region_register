cmake_minimum_required(VERSION 3.0.2)
project(mediassist3_region_register)

set(CMAKE_AUTOMOC ON) # For meta object compiler
set(CMAKE_AUTORCC ON) # Resource files
set(CMAKE_AUTOUIC ON) # UI files

SET(SRC_LIST 
    
    main.cpp 
    
    include/RegistrationThread.h
    
    include/GUI/pclviewer.h
    include/GUI/OptionsMenu.h

    include/PCL_Extensions/InteractorHandler.h
    include/PCL_Extensions/color_handlers.h
    include/PCL_Extensions/KdTreeSphereSearch.h
    src/PCL_Extensions/InteractorHandler.cpp

    src/GUI/pclviewer.cpp
    src/GUI/OptionsMenu.cpp
    
    src/VisHelper.cpp

    src/RegionContainer.cpp
    src/Utils.cpp
    
    src/IO/InputHandler.cpp 
    src/IO/OutputHandler.cpp 
    
    src/RegistrationThread.cpp
)

find_package(PCL 1.8 REQUIRED)
find_package(Qt5 REQUIRED Widgets)
find_package(VTK REQUIRED)

# Fix a compilation bug under ubuntu 16.04 (Xenial)
list(REMOVE_ITEM PCL_LIBRARIES "vtkproj4")

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

find_package(catkin REQUIRED COMPONENTS
  pcl_conversions
  pcl_ros
  roscpp
  sensor_msgs
)

catkin_package(
)

include_directories(
    ${catkin_INCLUDE_DIRS}
)

add_executable(region_register ${SRC_LIST} resources/resources.qrc)
add_executable(tool 
                tool.cpp 
                include/RegistrationThread.h 
                include/RegionContainer.h 
                include/Utils.h
                
                src/RegistrationThread.cpp 
                src/RegionContainer.cpp 
                src/Utils.cpp
                )
                
target_link_libraries(region_register ${PCL_LIBRARIES} ${catkin_LIBRARIES} Qt5::Widgets ${VTK_LIBRARIES} yaml-cpp)
target_link_libraries(tool ${PCL_LIBRARIES} Qt5::Widgets)

