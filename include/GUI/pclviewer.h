#pragma once

#include <QMainWindow>
#include <QtGui>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_picking_event.h>

#include <vtkRenderWindow.h>

#include <ros/ros.h>

#include "../PCL_Extensions/InteractorHandler.h"
#include "../VisHelper.h"
#include "../PCL_Extensions/KdTreeSphereSearch.h"
#include "../PCL_Extensions/color_handlers.h"
#include "../RegionContainer.h"
#include "../RegistrationThread.h"
#include "../Utils.h"
#include "../IO/OutputHandler.h"
#include "../PCL_Extensions/color_handlers.h"
#include "OptionsMenu.h"

namespace Ui
{
  class PCLViewer;
}

// This class handles the GUI interaction and calls the registration and visualization helper classes
class PCLViewer : public QMainWindow
{
  Q_OBJECT

public:
    explicit PCLViewer (pcl::PointCloud<pcl::PointXYZRGB>::Ptr sourceCloud, QWidget *parent = 0);
    ~PCLViewer ();
    
    // Return if the user is currently in register mode
    bool registering() { return isRegistering; }
    
    // Update the right cloud every time the ROS callback comes in
    void updateTargetCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr new_cloud);
    
    // Set the correspondences via mouse input
    void setSrcCorrespondenceRegion(const pcl::visualization::MouseEvent&, void*);
    void setTrgtCorrespondenceRegion(const pcl::visualization::MouseEvent&, void*);
    
    // Return the viewers
    pcl::visualization::PCLVisualizer::Ptr getLeftViewer() { return viewer_left; }
    pcl::visualization::PCLVisualizer::Ptr getRightViewer() { return viewer_right; }
  
// The functions called if the corresponding GUI elements are used
public Q_SLOTS:
    void startButtonPressed();
    void endButtonPressed();
    void addRegionButtonPressed();
    void removeRegionButtonPressed();
    void registerButtonPressed();
    void publishButtonPressed();
    void saveButtonPressed();
    void optionsButtonPressed();
    void listEntrySelected();
    
public slots:
    // Update visual elements while registering
    void updateProgress(
        int iteration,
        pcl::PointCloud<pcl::PointXYZRGB> t_src_cloud, 
        pcl::PointCloud<pcl::PointXYZRGB> t_src_regions,
        std::vector<RegionContainerRef> c_regContainers
        );
    // Some cleanup after a registration has been performed
    void handleRegDone(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_src_cloud,
        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> t_src_regions_vec,
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4 transformation
    );
    // Reset the thread adress
    void terminateThread();

protected:
    
    // empty parameters to call viewer constructor with
    int viewer_args_length = 0;
    char** viewer_args = {NULL};

    // point sizes
    int initial_point_size = 3;
    int highlight_point_size = 5;

    // interactor styles
    InteractorHandler* interactor_style_left;
    InteractorHandler* interactor_style_right;
    InteractorHandler* interactor_style_bottom;

    // The viewers in the different widgets
    pcl::visualization::PCLVisualizer::Ptr viewer_left;
    pcl::visualization::PCLVisualizer::Ptr viewer_right;
    pcl::visualization::PCLVisualizer::Ptr viewer_bottom;
    // The original source and target clouds
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud;
    std::string source_id = "source_cloud";
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_cloud;
    std::string target_id = "target_cloud";
    // The clouds used to highlight points
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr highl_source_cloud;
    std::string highl_source_id = "highl_source_cloud";
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr highl_target_cloud;
    std::string highl_target_id = "highl_target_cloud";

    // A copy of the target cloud used for the registration
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr c_target_cloud;
    
    // The indexed versions of the point clouds
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_source_cloud;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_target_cloud;

    // The original source and target clouds
    std::string source_rayCast_cloud_id = "rayCast_source";
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr source_rayCast_cloud;
    PointCloudColorHandlerCustomAlpha<pcl::PointXYZRGBL> source_rayCast_cloud_color;
    std::string target_rayCast_cloud_id = "rayCast_target";
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr target_rayCast_cloud;
    PointCloudColorHandlerCustomAlpha<pcl::PointXYZRGBL> target_rayCast_cloud_color;

    // curvature color handler for preoperative
    PointCloudColorHandlerCurvature<pcl::PointXYZRGB> source_color;

    // Override Qt close event
    void closeEvent(QCloseEvent *event);
    
private:
    // UI pointer
    Ui::PCLViewer *ui;
    // The model for correspondence list entrys
    QStringListModel *model;
    // Size policy so all layouts remain at their positions when the registration layout widget is hidden
    QSizePolicy *sizePolicy;
    
    // Options menu instance
    OptionsMenu *options;
    
    // Is the user in register mode or not
    bool isRegistering = false;
    // Is the bottom viewport set
    bool isBottomViewportSet = true;
    // Is the first ROS callback initiated
    bool firstCallbackInitiated = false;
    // Checks if an entry in the list has been selected
    bool isListEntrySelected = false;
    // prevent to many raycasts
    std::chrono::time_point<std::chrono::system_clock> lastSelect = std::chrono::time_point<std::chrono::system_clock>::min();
    long clickDebounceTimeMillis = 5;
    
    // Octree handler instance
    std::shared_ptr<KdTreeSphereSearch<pcl::PointXYZRGB>> m_oct_source;
    std::shared_ptr<KdTreeSphereSearch<pcl::PointXYZRGB>> m_oct_target;
    // Output handler instance, used for the output of the transformation
    OutputHandlerRef m_out;
    
    // The vector storing the region containers
    std::vector<RegionContainerRef> regContainers;
    
    // Thread pointer to control the registration thread outsife of the registration function
    RegistrationThread *m_t = nullptr;
    
    // Update the bottom viewport
    void updateBottomViewport(bool isFirstCall);
    
    // Call this function if the user clicks to highlight a region
    void selectRegion(const pcl::visualization::MouseEvent& event, bool isSource);
    // Remove highlighted clouds
    void removeHighlightedClouds();

    void setupInteractorRegionSelect();
};
using PCLViewerRef = std::shared_ptr<PCLViewer>;
