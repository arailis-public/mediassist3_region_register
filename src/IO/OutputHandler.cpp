 #include "../../include/IO/OutputHandler.h"

OutputHandler::OutputHandler () :
    output_source_cloud(new pcl::PointCloud<pcl::PointXYZRGB>())
{
    pub = nh.advertise<sensor_msgs::PointCloud2> ("rigid_registration", 1, true);
}

OutputHandler::~OutputHandler() {
}

/**
 * @brief Set the clouds and transformation before sending or saving
 * @param source the transformed preoperative cloud
 * @param t_regions_src input vector with transformed source regions
 * @param newTransformation the new transformation matrix that will be used
 */
void OutputHandler::setCloudsAndTransformation(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source,
        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> t_regions_src,
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4 newTransformation
        ) {
    // Set clouds and regions
    *output_source_cloud = *source;
    for(int i = 0; i < t_regions_src.size(); i++) {
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr newCloud(new pcl::PointCloud<pcl::PointXYZRGB>());
        *newCloud = *t_regions_src.at(i);
        transformed_regions_src.push_back(newCloud);
    }
    // Now the transformation
    transformation = newTransformation;

    // Print the transformation to send
    ROS_DEBUG_STREAM("Transformation:");
    ROS_DEBUG_STREAM(transformation(0, 0) << " " << transformation(0, 1) << " " << transformation(0, 2) << " " << transformation(0, 3));
    ROS_DEBUG_STREAM(transformation(1, 0) << " " << transformation(1, 1) << " " << transformation(1, 2) << " " << transformation(1, 3));
    ROS_DEBUG_STREAM(transformation(2, 0) << " " << transformation(2, 1) << " " << transformation(2, 2) << " " << transformation(2, 3));
    ROS_DEBUG_STREAM(transformation(3, 0) << " " << transformation(3, 1) << " " << transformation(3, 2) << " " << transformation(3, 3));
}

/**
 * @brief Send the transformation and transformed preoperative cloud inside the ROS pipeline
 */
void OutputHandler::sendData() {
    // Broadcaster and geometry transformation instance
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    geometry_msgs::TransformStamped transformStamped;
    // Return current time and set ids
    transformStamped.header.stamp = ros::Time::now();
    transformStamped.header.frame_id = "map";
    transformStamped.child_frame_id = "ct";
    // Quaternion instance, transmit values
    Eigen::Quaternionf quat(transformation.block<3,3>(0,0));
    // Handle transformation and rotation parameters
    transformStamped.transform.translation.x = transformation(0,3);
    transformStamped.transform.translation.y = transformation(1,3);
    transformStamped.transform.translation.z = transformation(2,3);
    transformStamped.transform.rotation.x = quat.x();
    transformStamped.transform.rotation.y = quat.y();
    transformStamped.transform.rotation.z = quat.z();
    transformStamped.transform.rotation.w = quat.w();

    // Send the transformation
    static_broadcaster.sendTransform(transformStamped);

    // Transfer the data to the output cloud and convert it to a ros msg, then publish the msg
    sensor_msgs::PointCloud2 msg;
    pcl::toROSMsg(*output_source_cloud, msg);
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = "map";
    pub.publish(msg);
    // Quaternion corrdinated for debug mode
    ROS_INFO_STREAM("Publishing.");
    ROS_DEBUG_STREAM("Quaternion coordinates:");
    ROS_DEBUG_STREAM(quat.x());
    ROS_DEBUG_STREAM(quat.y());
    ROS_DEBUG_STREAM(quat.z());
    ROS_DEBUG_STREAM(quat.w());
    // Warn if no subscribers are present
    if(pub.getNumSubscribers() == 0) {
        ROS_WARN_STREAM("No subscribers were detected!");
    }
}

/**
 * @brief This function saves a yaml file out ofthe transformation and a pcd file out of the point cloud
 * @param filePath path where the files are stored
 * @param output_target_cloud generated target cloud
 * @param regContainers region containers which source and target regions are stored
 */
void OutputHandler::saveData(
        std::string filePath,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr output_target_cloud,
        std::vector<RegionContainerRef> &regContainers) {
    // Write the clouds to file
    // Transformed source and target cloud
    pcl::io::savePCDFileASCII(filePath + "source_cloud.pcd", *output_source_cloud);
    pcl::io::savePCDFileASCII(filePath + "target_cloud.pcd", *output_target_cloud);
    // Source and target regions
    for(int i = 0; i < regContainers.size(); i++) {
        pcl::io::savePCDFileASCII(filePath + "src_region" + std::to_string(i) + ".pcd", *regContainers.at(i)->getIndexedSourceRegion());
        pcl::io::savePCDFileASCII(filePath + "target_region" + std::to_string(i) + ".pcd", *regContainers.at(i)->getIndexedTargetRegion());
    }
    for(int i = 0; i < transformed_regions_src.size(); i++) {
        pcl::io::savePCDFileASCII(filePath + "src_region_transformed" + std::to_string(i) + ".pcd", *transformed_regions_src.at(i));
    }
    ROS_INFO_STREAM("Saved pcd files.");

    YAML::Node yaml = createYAML();
    // Create a yaml file and write the parent node into it
    std::ofstream fout(filePath + "transformation.yaml");
    fout << yaml;
    ROS_INFO_STREAM("Saved yaml file output_transformation.yaml.");
}

/**
 * @brief Creates a YAML node
 */
YAML::Node OutputHandler::createYAML() {
    // Data nodes
    YAML::Node parentNode;
    YAML::Node transformationNode;
    YAML::Node translationNode;
    YAML::Node rotationNode;
    YAML::Node timeNode;
    // The parent node gets all transformation details
    parentNode["transformation"] = transformationNode;
    // Transfer th translation data
    transformationNode["translation"] = translationNode;
    translationNode["x"] = transformation(0,3);
    translationNode["y"] = transformation(1,3);
    translationNode["z"] = transformation(2,3);
    // Same for rotation 
    transformationNode["rotation"] = rotationNode;
    Eigen::Quaternionf quat( transformation.block<3,3>(0,0) );
    rotationNode["x"] = quat.x();
    rotationNode["y"] = quat.y();
    rotationNode["z"] = quat.z();
    rotationNode["w"] = quat.w();
    // Now the ucrrent ROS time
    transformationNode["time"] = timeNode;
    timeNode["timeSeconds"] = ros::Time::now().toSec();
    timeNode["timeNanoSeconds"] = ros::Time::now().toNSec();

    // Return final node
    return parentNode;
}
