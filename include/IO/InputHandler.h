#pragma once

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl_conversions/pcl_conversions.h>

#include "../GUI/pclviewer.h"

// This class handles the ROS input delivered by other ROS nodes
class InputHandler {

private:
    ros::NodeHandle nh;
    ros::Subscriber sub;
    // Viewer instance to check if user is currently in the register mode
    PCLViewerRef m_pclV;
    // The cloud transferred to the target viewport
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
    
public:
    InputHandler(PCLViewerRef pclV);
    ~InputHandler();
    
    // Callback function
    void callback (const sensor_msgs::PointCloud2ConstPtr& input);
};
using InputHandlerRef = std::shared_ptr<InputHandler>;
