#include "../../include/GUI/pclviewer.h"
#include "ui_pclviewer.h"

#include <pcl/visualization/interactor_style.h>
#include <vtkCamera.h>
#include <vtkPointPicker.h>

PCLViewer::PCLViewer (
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr sourceCloud,
    QWidget *parent
) :
    // The source cloud is the cloud loaded from the console or provided via the parameter server
    source_cloud(sourceCloud),
    
    // Allocate all cloud pointers
    target_cloud(new pcl::PointCloud<pcl::PointXYZRGB>()),  
    highl_source_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    highl_target_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    c_target_cloud(new pcl::PointCloud<pcl::PointXYZRGB>()),
    indx_source_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    indx_target_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>()),
    source_rayCast_cloud {new pcl::PointCloud<pcl::PointXYZRGBL>()},
    target_rayCast_cloud {new pcl::PointCloud<pcl::PointXYZRGBL>()},
    source_rayCast_cloud_color {source_rayCast_cloud, 0, 255, 0, 60},
    target_rayCast_cloud_color {target_rayCast_cloud, 0, 255, 0, 60},
    // Interactor Styles
    interactor_style_left(new InteractorHandler()),
    interactor_style_right(new InteractorHandler()),
    interactor_style_bottom(new InteractorHandler()),
    // Allocate the viewer pointers
    viewer_left(new pcl::visualization::PCLVisualizer (viewer_args_length, viewer_args,"viewer left", interactor_style_left, false)),
    viewer_right(new pcl::visualization::PCLVisualizer (viewer_args_length, viewer_args,"viewer right", interactor_style_right, false)),
    viewer_bottom(new pcl::visualization::PCLVisualizer (viewer_args_length, viewer_args,"viewer bottom", interactor_style_bottom, false)),
    
    // Allocate the UI pointer
    QMainWindow (parent),
    ui (new Ui::PCLViewer)
{
    m_out = std::make_shared<OutputHandler>();
    m_oct_source = std::make_shared<KdTreeSphereSearch<pcl::PointXYZRGB>>();
    m_oct_target = std::make_shared<KdTreeSphereSearch<pcl::PointXYZRGB>>();

    // Allocate the list model
    model = new QStringListModel(this);
    
    // Setup the UI, list model and title
    ui->setupUi (this);
    ui->listView->setModel(model);

    // The list will be non-editable
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    // Set window title and hit destructor when closing
    this->setWindowTitle ("PointRegister");
    
    // Set the policy so that all layouts retain their size
    sizePolicy = new QSizePolicy(ui->layoutRegManaging->sizePolicy());
    sizePolicy->setRetainSizeWhenHidden(true);
    ui->layoutRegManaging->setSizePolicy(*sizePolicy);
    // Hide the layout for registration managing because the program starts in update mode
    ui->layoutRegManaging->hide();
    // Also, we hide the progress bar
    ui->progressBar->hide();
    
    // Create options menu instance
    options = new OptionsMenu(this);
    // Center options widget
    options->setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, options->minimumSize(), qApp->desktop()->availableGeometry()));
    
    // Connect all buttons
    connect (ui->buttonStartRegister,  SIGNAL (clicked ()), this, SLOT (startButtonPressed ()));
    connect (ui->buttonEnd,  SIGNAL (clicked ()), this, SLOT (endButtonPressed ()));
    connect (ui->buttonAddRegion,  SIGNAL (clicked ()), this, SLOT (addRegionButtonPressed ()));
    connect (ui->buttonRemoveRegion,  SIGNAL (clicked ()), this, SLOT (removeRegionButtonPressed ()));
    connect (ui->buttonRegister,  SIGNAL (clicked ()), this, SLOT (registerButtonPressed ()));
    connect (ui->buttonPublish,  SIGNAL (clicked ()), this, SLOT (publishButtonPressed ()));
    connect (ui->buttonSave,  SIGNAL (clicked ()), this, SLOT (saveButtonPressed ()));
    connect (ui->buttonOptions,  SIGNAL (clicked ()), this, SLOT (optionsButtonPressed ()));
    connect (ui->listView,  SIGNAL (clicked (QModelIndex)), this, SLOT (listEntrySelected ()));
    
    // Adds image for options button
    QPixmap pixmap(":/images/Gear.png");
    QIcon ButtonIcon(pixmap);
    ui->buttonOptions->setIcon(ButtonIcon);
    ui->buttonOptions->setIconSize(QSize(16,16));
    
    ui->buttonRegister->setEnabled(true);
    // These buttons can't be used because currently no regions were created
    ui->buttonSave->setEnabled(false);
    ui->buttonPublish->setEnabled(false);
    ui->buttonRemoveRegion->setEnabled(false);

    // Handle the left widget and viewer, set the left original cloud to this widget
    ui->qvtkWidgetLeft->SetRenderWindow (viewer_left->getRenderWindow ());

    viewer_left->setupInteractor (ui->qvtkWidgetLeft->GetInteractor (), ui->qvtkWidgetLeft->GetRenderWindow ());
    source_color.setInputCloud(sourceCloud);
    double minColor[3] {0, 180, 255}; // low curvature
    double maxColor[3] {0, 20, 255}; // high curvature
    source_color.setInterpolationBounds(minColor,maxColor);
    viewer_left->addPointCloud (source_cloud, source_color, source_id);
    viewer_left->resetCamera();
    viewer_left->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, initial_point_size, source_id);
    // update view
    ui->qvtkWidgetLeft->update();
    
    // Display the right and bottom widget, but they don't display anything right at the start
    ui->qvtkWidgetRight->SetRenderWindow (viewer_right->getRenderWindow());
    viewer_right->setupInteractor (ui->qvtkWidgetRight->GetInteractor(), ui->qvtkWidgetRight->GetRenderWindow());
    ui->qvtkWidgetRight->update();

    ui->qvtkWidgetBottom->SetRenderWindow (viewer_bottom->getRenderWindow());
    viewer_bottom->setupInteractor(ui->qvtkWidgetBottom->GetInteractor(), ui->qvtkWidgetBottom->GetRenderWindow());
    ui->qvtkWidgetBottom->update();
}

// Initiate the highlighting of selected regions
// true and false are to determine if the event comes from the source or target cloud
void PCLViewer::setSrcCorrespondenceRegion(const pcl::visualization::MouseEvent& event, void*) {
    selectRegion(event, true);
};

void PCLViewer::setTrgtCorrespondenceRegion(const pcl::visualization::MouseEvent& event, void*) {
    selectRegion(event, false);
};


/**
 * @brief Updates the target point cloud everytime the ROS callback is initiated
 * @param new_cloud The cloud used to update the target cloud
 */
void PCLViewer::updateTargetCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr new_cloud) {
    // Set the target cloud to the input
    *target_cloud = *new_cloud;
    
    // Check if the callback was initiated for the first time
    if(!firstCallbackInitiated) {
        firstCallbackInitiated = true;
        // If so, add the cloud to the viewer and set it's rendering properties
        VisHelper::checkColorAndAddCloud(viewer_right, target_cloud, target_id);
        viewer_right->resetCamera ();
        viewer_right->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, initial_point_size, target_id);
        ui->qvtkWidgetRight->update ();
        return;
    }

    // Now update the cloud
    VisHelper::checkColorAndAddCloud(viewer_right, target_cloud, target_id);
    ui->qvtkWidgetRight->update ();
}

/**
 * @brief Setup the bottom viewport if the user switches to the registration mode
 * @param isFirstCall Check if this function is called for the first time
 */
void PCLViewer::updateBottomViewport(bool isFirstCall) {
    // Copy the values of the target cloud
    *c_target_cloud = *target_cloud;
    // If this function is called for the first time, setup the bottom viewport
    if(isFirstCall) {
        // Add the cloud, set the properties and update the widget
        VisHelper::checkColorAndAddCloud(viewer_bottom, c_target_cloud, target_id);
        viewer_bottom->resetCamera ();
        viewer_bottom->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, initial_point_size, target_id);
        ui->qvtkWidgetBottom->update();
    // In all other cases, just update this viewport and it's widget
    } else {
        VisHelper::checkColorAndAddCloud(viewer_bottom, c_target_cloud, target_id);
        ui->qvtkWidgetBottom->update ();
    }
}

/**
 * @brief This function sets to the registering mode
 */
void PCLViewer::startButtonPressed() {
    // The registration process can't be done if no target cloud has been set yet. Thus we're aborting if this is the case.
    if(!firstCallbackInitiated) {
        ROS_WARN_STREAM("No target cloud has been set. Aborting.");
        return;
    }
    // Set the registering mode to true so no more updates are coming in
    isRegistering = true;
    // Update the third viewport and set the first call variable to false because the viewport needs to be created only once
    updateBottomViewport(isBottomViewportSet);
    if (isBottomViewportSet) {
        isBottomViewportSet = false;
    }
    // Set the indexed clouds
    Utils::copyPointCloudWithIndex(source_cloud, indx_source_cloud);
    Utils::copyPointCloudWithIndex(target_cloud, indx_target_cloud);
    // Set the octree for the right cloud
    m_oct_source->setInputCloud(source_cloud);
    m_oct_target->setInputCloud(target_cloud);
    // set circle properties of interactor style
    setupInteractorRegionSelect();
    // Now show the registration managing widget
    ui->layoutRegManaging->show();
    // Right at the start, a region is created so the user won't have to click on the add region button
    addRegionButtonPressed();
    // Disable the start button until the registration mode is ended
    ui->buttonStartRegister->setEnabled(false);

    // add highlight clouds for raycast selection
    viewer_left->addPointCloud(source_rayCast_cloud, source_rayCast_cloud_color, source_rayCast_cloud_id);
    viewer_left->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, highlight_point_size, source_rayCast_cloud_id);
    viewer_right->addPointCloud(target_rayCast_cloud, source_rayCast_cloud_color, target_rayCast_cloud_id);
    viewer_right->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, highlight_point_size, target_rayCast_cloud_id);

}

void PCLViewer::setupInteractorRegionSelect() {
    // trigger win_->render whenever circle is changed
    interactor_style_left->coupleCircleRadius(interactor_style_right);
    interactor_style_right->coupleCircleRadius(interactor_style_left);

    auto maxRadius = 0.1f; // 10 cm
    interactor_style_left->SetMinRadius(m_oct_source->getRaycastResolution() * sqrt(3));
    interactor_style_left->SetRadius(maxRadius * 0.3);
    interactor_style_left->SetMaxRadius(maxRadius);
    interactor_style_right->SetMinRadius(m_oct_target->getRaycastResolution() * sqrt(3));
    interactor_style_right->SetRadius(interactor_style_left->GetRadius());
    interactor_style_right->SetMaxRadius(maxRadius);

    // Enable region select for both interactor styles (displays circle, enables selection mode)
    interactor_style_left->StartRegionSelect();
    interactor_style_right->StartRegionSelect();
}


/**
 * @brief This function sets to the updating mode and hides the registration interface
 */
void PCLViewer::endButtonPressed() {
    // The user is not in registration mode anymore
    isRegistering = false;
    // Terminate running registration thread and set member thread
    terminateThread();
    m_t = nullptr;
    // Hide the progress bar if the registration thread was active
    if(ui->progressBar->isVisible()) {
        ui->progressBar->hide();
    }
    // Remove all regions and reset the stored clouds
    regContainers.clear();
    removeHighlightedClouds();
    viewer_left->removePointCloud(source_rayCast_cloud_id);
    viewer_right->removePointCloud(target_rayCast_cloud_id);
    // Remove all clouds in the bottom viewer
    viewer_bottom->removeAllPointClouds();

    // Update all widgets
    ui->qvtkWidgetLeft->update ();
    ui->qvtkWidgetRight->update ();
    ui->qvtkWidgetBottom->update ();
    // Remove all the correspondences out of the list
    model->removeRows(0, model->rowCount());
    // Disable the buttons because all correspondences are removed
    ui->buttonRegister->setEnabled(false);
    ui->buttonSave->setEnabled(false);
    ui->buttonPublish->setEnabled(false);
    // Regions can be added again
    ui->buttonAddRegion->setEnabled(true);
    // No more list entry is selected
    isListEntrySelected = false;
    // Hide the layout widget, but keep the other layouts at their places
    ui->layoutRegManaging->hide();
    // Reenable the start button
    ui->buttonStartRegister->setEnabled(true);

    // disable region select for both interactor styles (hides circle, disables selection mode)
    interactor_style_left->StopRegionSelect();
    interactor_style_right->StopRegionSelect();
}

/**
 * @brief This function creates a new region container and stores it in a list
 */
void PCLViewer::addRegionButtonPressed() {
    // Create a new region container to store the regions
    RegionContainerRef regionContainer = std::make_shared<RegionContainer>();
    // Push it back into the vector
    regContainers.push_back(regionContainer);
    // Create the list entry
    if(model->insertRow(model->rowCount())) {
        ui->buttonRemoveRegion->setEnabled(true);
        model->setData(model->index(model->rowCount() - 1, 0), QVariant::fromValue(model->rowCount()));
    }
    // If there were no regions before, the register button has to be reenabled
    if(model->rowCount() == 1) {
        ui->buttonRegister->setEnabled(true);
    }
    // Select the newly added row
    ui->listView->setCurrentIndex(model->index(model->rowCount() - 1, 0));
    listEntrySelected();
}

/**
 * @brief Removes a correspondence out of the point clouds and the correspondence list by index
 */
void PCLViewer::removeRegionButtonPressed() {
    if(isListEntrySelected) {
        // Get the index of the row to remove
        int rowNumberToRemove = ui->listView->currentIndex().row();
        // First, remove the corresponding element out of the vector
        removeHighlightedClouds();
        regContainers.erase(regContainers.begin() + rowNumberToRemove);
        // Now update the stored clouds

        // Then remove the selected row out of the list
        model->removeRows(ui->listView->currentIndex().row(), 1);
        // If no regions remain, disable the button for region removal and registering
        if(model->rowCount() == 0) {
            ui->buttonRemoveRegion->setEnabled(false);
            ui->buttonRegister->setEnabled(false);
        }
        isListEntrySelected = false;
        // Update the widgets
        ui->qvtkWidgetLeft->update ();
        ui->qvtkWidgetRight->update ();
        ui->qvtkWidgetBottom->update ();
        isListEntrySelected = false;
        
        // Get the current numbers of rows
        int rowNumbers = model->rowCount();
        // Clear all list entries
        model->removeRows(0, model->rowCount());
        // Rename the remaining row entrys so that the consistency is secured
        for(int i = 0; i < rowNumbers; i++) {
            if(model->insertRow(model->rowCount())) {
                model->setData(model->index(model->rowCount() - 1, 0), i + 1);
            }
        }
        // Now a row is selected
        if(rowNumberToRemove == 0) {
            // Select the new first row if the first one was selected 
            ui->listView->setCurrentIndex(model->index(0, 0));
        } else {
            // Otherwise select the row before the remoed one
            ui->listView->setCurrentIndex(model->index(rowNumberToRemove - 1, 0));
        }
        // Visualize the data inside the selected row
        if(model->rowCount() != 0) {
            listEntrySelected();
        }
        return;
    }
    // Abort if no list entry to remove was selected
    ROS_INFO_STREAM("Please select a correspondence in the list before removal.");
}

/**
 * @brief Performs a registration.
 */
void PCLViewer::registerButtonPressed() {
    // At first, we check if every created region in the list has point clouds in the source and target viewport
    for(int i = 0; i < regContainers.size(); i++) {
        // First, we try to set the unlabeled regions
        regContainers.at(i)->setUnlabeledRegions();
        // If this is not the case, abort the registration
        if(regContainers.at(i)->getSourceRegion()->empty() || regContainers.at(i)->getTargetRegion()->empty()) {
            ROS_INFO_STREAM("Please make sure that every region contains at least one point cloud.");
            return;
        }
    }
    // Now that the registration has been started, we don't want the user to mess arround with the correspondences
    // So the buttons to manipulate the regions and to do the registration are locked
    ui->buttonAddRegion->setEnabled(false);
    ui->buttonRemoveRegion->setEnabled(false);
    ui->buttonRegister->setEnabled(false);
    ui->buttonSave->setEnabled(false);
    ui->buttonPublish->setEnabled(false);
    ui->buttonOptions->setEnabled(false);
    
    // The progress bar is shown now
    ui->progressBar->show();
    // In case we start again, set the progress bar to zero
    ui->progressBar->setValue(0);
    // If a registration has been performed already, remove it
    if(viewer_bottom->contains("regions intra")) {
        viewer_bottom->removePointCloud("regions intra");
    }
    
    // Create the registration thread
    m_t = new RegistrationThread(options->getIterationCount(), options->getError(), source_cloud, indx_source_cloud, target_cloud, indx_target_cloud, regContainers);
    // Connect deleting function and progress bar handling
    connect(m_t, &RegistrationThread::finished, m_t, &QObject::deleteLater);
    connect(m_t, &RegistrationThread::updateProgress, this, &PCLViewer::updateProgress);
    connect(m_t, &RegistrationThread::regDone, this, &PCLViewer::handleRegDone);
    // Now the adress is given to the member thread
    // Start the registration
    m_t->start();
}

/**
 * @brief Sends the transformation and transformed source cloud via ROS (if the registration has been done)
 */
void PCLViewer::publishButtonPressed() {
    m_out->sendData();
}

/**
 * @brief Saves the transformation, the unmodified regions, the transformed source regions and source cloud to a specified file location
 */
void PCLViewer::saveButtonPressed() {
    // File dialog to save files
    QString qs = QFileDialog::getSaveFileName(this, "Save files", "output_", "");
    // Return if nothing was provided
    if (qs.isEmpty()) {
        return;
    } else {
        // Else create a standard string and use it to save the files
        std::string filePath = qs.toUtf8().constData();
        // Now save
        m_out->saveData(filePath, target_cloud, regContainers);
    }
}

/**
 * @brief Open options menu
 */
void PCLViewer::optionsButtonPressed() {
    options->show();
}

/**
 * @brief Every time the user clicks on a stored region container in the list, it's points will be highlighted
 */
void PCLViewer::listEntrySelected() {
    // Update the viewer
    highl_source_cloud = regContainers.at(ui->listView->currentIndex().row())->getIndexedSourceRegion();
    highl_target_cloud = regContainers.at(ui->listView->currentIndex().row())->getIndexedTargetRegion();
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBL> colorSrc(highl_source_cloud, 255, 255, 255);
    VisHelper::updateCloud(viewer_left, highl_source_cloud, colorSrc, highl_source_id, highlight_point_size);
    viewer_left->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, highlight_point_size, highl_source_id);

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBL> colorTrgt(highl_target_cloud, 255, 255, 255);
    VisHelper::updateCloud(viewer_right, highl_target_cloud, colorTrgt, highl_target_id, highlight_point_size);
    viewer_right->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, highlight_point_size, highl_target_id);

    // Now the removal of a correspondence is possible
    isListEntrySelected = true;
    ui->qvtkWidgetLeft->update();
    ui->qvtkWidgetRight->update();
}

/**
 * @brief Updates visual elements as the registration proceeds
 * @param t_source_cloud transformed source cloud
 * @param t_regions_src contains transformed source regions
 * @param c_regContainers copy of region containers
 */
void PCLViewer::updateProgress(
        int iteration,
        pcl::PointCloud<pcl::PointXYZRGB> t_preoperative_cloud, 
        pcl::PointCloud<pcl::PointXYZRGB> t_regions_src,
        std::vector<RegionContainerRef> c_regContainers
        ) {
    // If the thread has been terminated, don't do anymore visualizations
    if(m_t != nullptr) {
        // Set the progress bar value
        ui->progressBar->setValue((iteration) / (options->getIterationCount() / 100));
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_preoperative_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_regions_src_ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
        *t_preoperative_cloud_ptr = t_preoperative_cloud;
        *t_regions_src_ptr = t_regions_src;
        // Now visualize the transformed cloud
        VisHelper::visualizeRegistration(t_preoperative_cloud_ptr, t_regions_src_ptr, regContainers, viewer_bottom);
        ui->qvtkWidgetBottom->update();
    }
}

/**
 * @brief Handles some functions after a registration has been performed
 * @param t_source_cloud transformed source cloud
 * @param transformed_regions_src Vector containing transformed source regions
 * @param transformation Final transformation
 */
void PCLViewer::handleRegDone(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_source_cloud,
        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> transformed_regions_src,
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4 transformation
    ) {
    // Set the data in the output handler
    m_out->setCloudsAndTransformation(t_source_cloud, transformed_regions_src, transformation);
    // Hide the progress bar
    ui->progressBar->hide();
    // Reenable the buttons
    ui->buttonAddRegion->setEnabled(true);
    ui->buttonRemoveRegion->setEnabled(true);
    ui->buttonRegister->setEnabled(true);
    ui->buttonSave->setEnabled(true);
    ui->buttonPublish->setEnabled(true);
    ui->buttonOptions->setEnabled(true);
    // reset member thread
    m_t = nullptr;
}

/**
 * @brief Main function to select a region in the viewports
 * @param event used MouseEvent
 * @param isSource is the selected region in the source or target viewport
 */
void PCLViewer::selectRegion(const pcl::visualization::MouseEvent& event, bool isSource) {
    // Type Alias
    using MouseEvent = pcl::visualization::MouseEvent;

    // only available during registration
    if (!isRegistering || ui->listView->currentIndex().row() < 0) {
        return;
    }

    // respect debounce to limit cpu usage
    if (std::abs(std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now() - lastSelect).count())
        < clickDebounceTimeMillis) {
        return;
    }

    bool isSelectionEvent = event.getButton() == MouseEvent::MouseButton::LeftButton && event.getType() == MouseEvent::Type::MouseButtonPress ||
            event.getType() == MouseEvent::MouseMove;

    // Filter out "wrong" buttons
    if (!isSelectionEvent) {
        return;
    }

    // setup variables depending on viewer
    auto viewer = isSource ? viewer_left : viewer_right;
    auto interactor_style = isSource ? interactor_style_left : interactor_style_right;
    auto indexed_cloud = isSource ? indx_source_cloud : indx_target_cloud;
    auto highlighted_cloud = isSource ? highl_source_cloud : highl_target_cloud;
    auto highlighted_cloud_id = isSource ? highl_source_id : highl_target_id;
    auto selectionCloud = isSource? source_rayCast_cloud : target_rayCast_cloud;
    auto selectionID = isSource ? source_rayCast_cloud_id : target_rayCast_cloud_id;
    auto selectionColor = isSource ? source_rayCast_cloud_color : target_rayCast_cloud_color;
    auto octree = isSource ? m_oct_source : m_oct_target;

    // setup event-dependent variables
    double mouse_x = event.getX();
    double mouse_y = event.getY();
    auto renderer = interactor_style->GetInteractor()->FindPokedRenderer((int) mouse_x, (int) mouse_y);
    auto camera = renderer->GetActiveCamera();
    auto ray_origin = camera->GetPosition();
    auto ray_target = interactor_style->MouseToWorldPoint(mouse_x, mouse_y);

    // get indices that are currently selected
    pcl::Indices pointIds = octree->sphereCast(ray_origin, ray_target, (float) *interactor_style->GetRadius());

    // copy selected points into new pointcloud
    pcl::copyPointCloud(*indexed_cloud, pointIds, *selectionCloud);

    if (event.getButton() == MouseEvent::MouseButton::LeftButton && event.getType() == MouseEvent::Type::MouseButtonPress ||
        event.getType() == MouseEvent::MouseMove && event.getSelectionMode())
    {
        // actual selection

        // toggle whether to remove or add the currently selected points
        if (interactor_style->GetInteractor()->GetShiftKey()) {
            // remove marking
            regContainers.at(ui->listView->currentIndex().row())->removePointsFromRegion(selectionCloud, isSource);
        } else {
            // remove duplicates from earlier selections
            for(auto & regContainer : regContainers) {
                regContainer->removePointsFromRegion(selectionCloud, isSource);
            }
            // add marking
            regContainers.at(ui->listView->currentIndex().row())->addPointsToRegion(selectionCloud, isSource);
        }
        // display selection
        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBL> white(highlighted_cloud, 255, 255, 255);
        viewer->updatePointCloud(highlighted_cloud, white, highlighted_cloud_id);
    }

    viewer->updatePointCloud<pcl::PointXYZRGBL>(selectionCloud, selectionColor, selectionID);
    double circleCenter[3];
    octree->getLastSearchPoint(circleCenter);
    interactor_style->SetCenter(circleCenter);

    lastSelect = std::chrono::system_clock::now();
}

/**
 * @brief Removes the highlighted regions out of the viewers
 */
void PCLViewer::removeHighlightedClouds() {
    viewer_left->removePointCloud(highl_source_id);
    viewer_right->removePointCloud(highl_target_id);
}

/**
 * @brief Handle the member thread adress is the program is terminated or the registration mode is ended
 */
void PCLViewer::terminateThread() {
    // If there was still a registration thread working in the background, we disconnect and kill it
    if(m_t != nullptr) {
        m_t->terminate();
        m_t->wait();
    } 
    // Then we delete it
    delete m_t;
}

/**
 * @brief Overriding closing event, deleting options menu if it is open
 */
void PCLViewer::closeEvent(QCloseEvent *event) {
    delete options;
}

PCLViewer::~PCLViewer () {
    terminateThread();
    delete ui;
    delete model;
    delete sizePolicy;
}
