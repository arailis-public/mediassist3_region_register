//
// Created by martin on 26.02.21.
//

#ifndef POINTREGISTER_INTERACTOR_STYLE_EXTENDED_H
#define POINTREGISTER_INTERACTOR_STYLE_EXTENDED_H

#include <map>
#include <vector>

#include <pcl/visualization/interactor_style.h>
#include <pcl/visualization/mouse_event.h>

#include <vtkRegularPolygonSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRendererCollection.h>

class InteractorHandler : public pcl::visualization::PCLVisualizerInteractorStyle
{
    /***
     * this class is intended to provide a custom interaction style with the pcl viewer
     * key configs are as follow:
     * LMB - nothing normally, can start state REGISTRATION_REGION_SELECT_CLICK if this interactor style IsRegionSelect
     * RMB - rotation of the pointcloud (what is usually bound to LMB)
     * Scroll - change Zoom level
     * Shift + Scroll - change circle size
     *
     * A circle is displayed indicating the current selection radius
     **/

public:
    using MouseEvent = pcl::visualization::MouseEvent;

    InteractorHandler();

    // ###  Mouse Events
    void OnLeftButtonDown () override;
    void OnLeftButtonUp () override;

    void OnRightButtonDown () override;
    void OnRightButtonUp () override;

    void OnMouseMove() override;
    void OnMouseWheelForward() override;
    void OnMouseWheelBackward() override;

    // no keyboard events for now
    void OnKeyDown() override;
    void OnKeyUp() override;

    // ### Region Selection Methods
    void StartRegionSelect();
    void StopRegionSelect();
    bool IsRegionSelect() {
        return regionSelect;
    }

    // ### running render on other interactors
    void coupleRenderWindow(InteractorHandler* i);
    void triggerCoupledRenderWindows();

    // ### Circle UI Methods
    void SetupCircleRenderers();
    void SetViewportsToDisplayCircle(int* rens_);
    void SetCircleDisplay(bool isDisplayed);

    void coupleCircleRadius(InteractorHandler* i) {
        coupledCircleRadius.push_back(i);
    }
    void triggerCoupledCircle();

    void RedrawCircle();
    void ResizeCircle(double factor);
    void SetCenter(double* coords);
    void SetNormal(double* coords);
    double* GetRadius() {
        return circle_radius;
    }
    void SetRadius(double* radius) {
        circle_radius = radius;
        RedrawCircle();
        triggerCoupledCircle();
    }
    void SetRadius(double radius) {
        *circle_radius = radius;
        RedrawCircle();
        triggerCoupledCircle();
    }
    void SetMaxRadius(double radius) {
        circle_max_radius = radius;
        RedrawCircle();
        triggerCoupledCircle();
    }
    void SetMinRadius(double radius) {
        circle_min_radius = radius;
        RedrawCircle();
        triggerCoupledCircle();
    }

    // ### Helper Methods
    double* MouseToWorldPoint(double mouse_x, double mouse_y);

    void MoveCircleToFocal();
    void CameraNormalToCircleNormal();

private:
    std::vector<InteractorHandler*> coupledInteractors;

    bool regionSelect = false;
    bool isSelecting = false;

    std::set<int> viewportsWithCircle = {};
    std::set<vtkSmartPointer<vtkRenderer>> circle_renderers {};
    std::vector<InteractorHandler*> coupledCircleRadius;
    vtkSmartPointer<vtkRegularPolygonSource> circle_source = vtkSmartPointer<vtkRegularPolygonSource>::New();
    vtkSmartPointer<vtkPolyDataMapper> circle_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    vtkSmartPointer<vtkActor> circle_actor = vtkSmartPointer<vtkActor>::New();
    bool circle_setup = false;
    double circle_center[3] = {0,0,0};
    double circle_normal[3] = {0,0,0};
    double* circle_radius = new double(1);
    double circle_max_radius = -1;
    double circle_min_radius = -1;
    bool IsMoveDuringSelect();

    bool isSet(double check) {
        return check != -1;
    }
};

#endif //POINTREGISTER_INTERACTOR_STYLE_EXTENDED_H

