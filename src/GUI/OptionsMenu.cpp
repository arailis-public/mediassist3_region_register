#include "../../include/GUI/OptionsMenu.h"

OptionsMenu::OptionsMenu(QWidget *parent) {
    // Set the spinboxes and then create the menu
    setSpinBoxes();
    createOptionsMenu();
    // Pass the spinbox values to the private member variables
    accepted();
}

/**
 * @brief Configures the spinboxes
 */
void OptionsMenu::setSpinBoxes() {
    // Allocate
    spinBoxIterationCount = new QSpinBox;
    spinBoxErrorValue = new QDoubleSpinBox;
    
    // Set the iteration counts box
    spinBoxIterationCount->setSuffix(" iterations");
    spinBoxIterationCount->setRange(100, 1000);
    spinBoxIterationCount->setSingleStep(10);
    spinBoxIterationCount->setValue(500);
    // Set error values spinbox
    spinBoxErrorValue->setDecimals(4);
    spinBoxErrorValue->setRange(0.0001, 0.01);
    spinBoxErrorValue->setSingleStep(0.0001);
    spinBoxErrorValue->setValue(0.0001);
}

/**
 * @brief Creates the options widget
 */
void OptionsMenu::createOptionsMenu() {
    
    // Set main layout
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    
    // Registration settings layout
    QGroupBox *registrationSettingsBox = new QGroupBox("Registration settings");
    QFormLayout *registrationSettingsLayout = new QFormLayout;
    registrationSettingsLayout->addRow(new QLabel("Maximum iteration count:"), spinBoxIterationCount);
    registrationSettingsLayout->addRow(new QLabel("Error value: "), spinBoxErrorValue);
    registrationSettingsBox->setLayout(registrationSettingsLayout);
    // Buttons
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accepted()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(rejected()));
    // Add a fancy title
    setWindowTitle("Options");
    
    // Add layouts
    mainLayout->addWidget(registrationSettingsBox);
    mainLayout->addWidget(buttonBox);
    // Set the layout as the main options layout
    setLayout(mainLayout);
}

/**
 * @brief Set the member variables after accepting 
 */
void OptionsMenu::accepted() {
    m_iter = spinBoxIterationCount->value();
    m_error = spinBoxErrorValue->value();
    // Close if done
    this->hide();
}

/**
 * @brief Resets the spinbox values with the member variable values if rejecting
 */
void OptionsMenu::rejected() {
    spinBoxIterationCount->setValue(m_iter);
    spinBoxErrorValue->setValue(m_error);
    // Then hide the widget
    this->hide();
}

