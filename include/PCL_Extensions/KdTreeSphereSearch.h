//
// Created by martin on 28.05.21.
//

#ifndef MEDIASSIST3_RELIABILITY_MODIFIER_OCTREESPHERESEARCH_H
#define MEDIASSIST3_RELIABILITY_MODIFIER_OCTREESPHERESEARCH_H

#include <pcl/octree/octree_search.h>
#include <pcl/search/kdtree.h>
#include <pcl/common/common.h>

#include "../Utils.h"


template <typename PointT>
class KdTreeSphereSearch: public pcl::search::KdTree<PointT>{
public:
    using PointCloud = pcl::PointCloud<PointT>;
    using PointCloudPtr = typename pcl::PointCloud<PointT>::Ptr;
    using AlignedPointTVector = std::vector<PointT, Eigen::aligned_allocator<PointT>>;

    KdTreeSphereSearch():  pcl::search::KdTree<PointT>() {};
    ~KdTreeSphereSearch() {
        delete[] lastSearchPoint;
    }
    void setInputCloud(PointCloudPtr cloud);

    double getRaycastResolution() {
        return this->raycastResolution;
    }

    void getLastSearchPoint(double* c) {
        c[0] = lastSearchPoint[0];
        c[1] = lastSearchPoint[1];
        c[2] = lastSearchPoint[2];
    }

    // sphereCast to find points on a ray
    pcl::Indices sphereCast(const double* origin, const double* target, float searchRadius);
    pcl::Indices sphereCast(const Eigen::Vector3f &origin, const Eigen::Vector3f &target, float searchRadius);


    bool boundingBoxRayIntersection(const Eigen::Vector3f &origin, const Eigen::Vector3f &direction, double border, float& tMin, float& tMax);

private:

    pcl::Indices inputIndices;
    std::vector<float> inputSquaredDistances;
    PointT inputMinPt, inputMaxPt;

    double* lastSearchPoint = new double[3];
    double raycastResolution = 0.002 / sqrt(3); // side length of voxel with 2mm diagonal length
    double raycastMaxDist = 2; // 2m max raymarch distance
};

template<typename PointT>
void KdTreeSphereSearch<PointT>::setInputCloud(KdTreeSphereSearch::PointCloudPtr cloud) {
    pcl::getMinMax3D(*cloud, inputMinPt, inputMaxPt);
    ROS_INFO_STREAM("setInputCloud;" << " bb min: " << inputMinPt.x << " " << inputMinPt.y << " " << inputMinPt.z
                                     << " bb max: " << inputMaxPt.x << " " << inputMaxPt.y << " " << inputMaxPt.z);
    pcl::search::KdTree<PointT>::setInputCloud(cloud);

}

template<typename PointT>
pcl::Indices
KdTreeSphereSearch<PointT>::sphereCast(const double *origin, const double *target,
                                       float searchRadius) {

    Eigen::Vector3f origin_{(float) origin[0], (float) origin[1], (float) origin[2]};
    Eigen::Vector3f target_{(float) target[0], (float) target[1], (float) target[2]};
    return sphereCast(origin_, target_, searchRadius);
}

template<typename PointT>
pcl::Indices
KdTreeSphereSearch<PointT>::sphereCast(const Eigen::Vector3f &origin,
                                       const Eigen::Vector3f &target,
                                       float searchRadius) {

    ROS_INFO_STREAM("Spherecast; starting "
                            << " from: " << origin.x() << " " << origin.y() << " " << origin.z()
                            << " to: " << target.x() << " " << target.y() << " " << target.z());
    // param setup
    inputIndices.clear();
    inputSquaredDistances.clear();
    lastSearchPoint[0] = target.x(); lastSearchPoint[1] = target.y(); lastSearchPoint[2] = target.z();

    if (searchRadius == 0)
    {
        ROS_INFO_STREAM("SphereCast; exit no radius");
        return inputIndices;
    }


    /*float tMin, tMax;
    if (!boundingBoxRayIntersection(origin, target-origin, searchRadius, tMin, tMax))
    {
        ROS_INFO_STREAM("Spherecast; exit no bounding box intersection"
                             << " bb min: " << inputMinPt.x << " " << inputMinPt.y << " " << inputMinPt.z
                             << " bb max: " << inputMaxPt.x << " " << inputMaxPt.y << " " << inputMaxPt.z);
        return inputIndices;
    }


    auto start = tMin > 0 ? origin + (target-origin).normalized() * tMin : origin; // if start point is already inside bounding box we do not need to move
    auto end = origin + (target-origin).normalized() * tMax;*/
    auto start = origin;
    auto end = target;
    auto direction = end-start;

    std::function<void(int*)> radiusSearch = [&](int point[3]){
        auto pointOnRay = Utils::projectOntoRay(start, end, Eigen::Vector3f(start.x() + point[0]*raycastResolution,
                                                                            start.y() + point[1]*raycastResolution,
                                                                            start.z() + point[2]*raycastResolution
        ));
        PointT pointOnRay_;
        pointOnRay_.x = pointOnRay.x();
        pointOnRay_.y = pointOnRay.y();
        pointOnRay_.z = pointOnRay.z();
        // perform radius search on projection
        lastSearchPoint[0] = pointOnRay_.x; lastSearchPoint[1] = pointOnRay_.y; lastSearchPoint[2] = pointOnRay_.z;
        this->radiusSearch(pointOnRay_, searchRadius, inputIndices, inputSquaredDistances);
        ROS_INFO_STREAM("RadiusSearch; at: "
                                << pointOnRay_.x << " " << pointOnRay_.y << " " << pointOnRay_.z
                                << " ;found " << inputIndices.size() << " points");
    };

    // bresenham
    // https://antofthy.gitlab.io/info/graphics/bresenham.procs
    int point[3] {0,0,0};
    int x_inc, y_inc, z_inc, dx, dy, dz, dx2, dy2, dz2, err_1, err_2;
    x_inc = (direction.x() < 0) ? -1 : 1;
    dx = abs(direction.x() / raycastResolution);
    y_inc = (direction.y() < 0) ? -1 : 1;
    dy = abs(direction.y() / raycastResolution);
    z_inc = (direction.z() < 0) ? -1 : 1;
    dz = abs(direction.z() / raycastResolution);
    dx2 = dx << 1;
    dy2 = dy << 1;
    dz2 = dz << 1;

    if ((dx >= dy) && (dx >= dz)) {
        err_1 = dy2 - dx;
        err_2 = dz2 - dx;
        int range = raycastMaxDist/raycastResolution;
        ROS_INFO_STREAM("Bresenham; start with range: " << range);
        for (int i = 0; i < range; i++) {
            ROS_INFO_STREAM("Bresenham; index "<< i);
            radiusSearch(point);
            // first non-empty set of points gets returned
            if(!inputIndices.empty()) {
                ROS_INFO_STREAM("Bresenham; exit at: "
                                        << lastSearchPoint[0] << " " <<lastSearchPoint[1] << " " << lastSearchPoint[2]
                                        << "; index: " << i);
                break;
            }

            if (err_1 > 0) {
                point[1] += y_inc;
                err_1 -= dx2;
            }
            if (err_2 > 0) {
                point[2] += z_inc;
                err_2 -= dx2;
            }
            err_1 += dy2;
            err_2 += dz2;
            point[0] += x_inc;
        }
    } else if ((dy >= dx) && (dy >= dz)) {
        err_1 = dx2 - dy;
        err_2 = dz2 - dy;
        int range = raycastMaxDist/raycastResolution;
        ROS_INFO_STREAM("Bresenham; start with range: " << range);
        for (int i = 0; i < range; i++) {
            ROS_INFO_STREAM("Bresenham; index "<< i);
            radiusSearch(point);
            // first non-empty set of points gets returned
            if(!inputIndices.empty()) {
                ROS_INFO_STREAM("Bresenham; exit at: "
                                        << lastSearchPoint[0] << " " <<lastSearchPoint[1] << " " << lastSearchPoint[2]
                                        << "; index: " << i);
                break;
            }

            if (err_1 > 0) {
                point[0] += x_inc;
                err_1 -= dy2;
            }
            if (err_2 > 0) {
                point[2] += z_inc;
                err_2 -= dy2;
            }
            err_1 += dx2;
            err_2 += dz2;
            point[1] += y_inc;
        }
    } else {
        err_1 = dy2 - dz;
        err_2 = dx2 - dz;
        int range = raycastMaxDist/raycastResolution;
        ROS_INFO_STREAM("Bresenham; start with range: " << range);
        for (int i = 0; i < range; i++) {
            ROS_INFO_STREAM("Bresenham; index "<< i);
            radiusSearch(point);
            // first non-empty set of points gets returned
            if(!inputIndices.empty()) {
                ROS_INFO_STREAM("Bresenham; exit at: "
                                        << lastSearchPoint[0] << " " <<lastSearchPoint[1] << " " << lastSearchPoint[2]
                                        << "; index: " << i);
                break;
            }

            if (err_1 > 0) {
                point[1] += y_inc;
                err_1 -= dz2;
            }
            if (err_2 > 0) {
                point[0] += x_inc;
                err_2 -= dz2;
            }
            err_1 += dy2;
            err_2 += dx2;
            point[2] += z_inc;
        }
    }

    if (inputIndices.empty())
    {
        ROS_INFO_STREAM("Spherecast; exit no points found via Bresenham + RadiusSearch");
        lastSearchPoint[0] = target.x(); lastSearchPoint[1] = target.y(); lastSearchPoint[2] = target.z(); // set to target
        return inputIndices;
    }

    // now we move to closest point on line and radius search from there
    int minPos = 0;
    double minDist = 1e10;

    for (int j = 0; j<inputSquaredDistances.size(); ++j)
    {
        if (inputSquaredDistances[j] < minDist)
        {
            minPos = j;
            minDist = inputSquaredDistances[j];
        }
    }

    auto closestPoint = this->input_->points[inputIndices[minPos]];
    ROS_INFO_STREAM("Spherecast; closest point for final Radiussearch at " << closestPoint.x << " " << closestPoint.y << " " << closestPoint.z);
    auto pointOnRay = Utils::projectOntoRay(origin, target, Eigen::Vector3f(closestPoint.x, closestPoint.y, closestPoint.z));

    PointT pointOnRay_;
    pointOnRay_.x = pointOnRay.x();
    pointOnRay_.y = pointOnRay.y();
    pointOnRay_.z = pointOnRay.z();

    // perform radius search on projection
    lastSearchPoint[0] = pointOnRay_.x; lastSearchPoint[1] = pointOnRay_.y; lastSearchPoint[2] = pointOnRay_.z;
    this->radiusSearch(pointOnRay_, searchRadius, inputIndices, inputSquaredDistances);
    ROS_INFO_STREAM("Spherecast; final Radiussearch at "
                            << lastSearchPoint[0] << " " << lastSearchPoint[1] << " " << lastSearchPoint[2] << " " <<
                            "found " << inputIndices.size() << " points");

    ROS_INFO_STREAM("Spherecast; final exit");
    return inputIndices;
}

template<typename PointT>
bool
KdTreeSphereSearch<PointT>::boundingBoxRayIntersection(const Eigen::Vector3f &origin, const Eigen::Vector3f &direction, double border,
                                                       float &tMin, float &tMax) {
    ROS_INFO_STREAM("boundingBoxRayIntersection; start " << "Ray "
                                                         << " from: " << origin.x() << " " << origin.y() << " " << origin.z()
                                                         << " direction: " << direction.x() << " " << direction.y() << " " << direction.z()
                                                         << " border: " << border
    );
    std::vector<Eigen::Vector3f> normals {
            {-1, 0, 0},
            {0, -1, 0},
            {0, 0, -1},
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
    };
    std::vector<float> steps(6);
    Eigen::Vector3f minPt {inputMinPt.x, inputMinPt.y, inputMinPt.z};
    Eigen::Vector3f maxPt {inputMaxPt.x, inputMaxPt.y, inputMaxPt.z};
    Utils::planeIntersection(normals[0], minPt,
                             origin, direction, steps[0]);
    Utils::planeIntersection(normals[1], minPt,
                             origin, direction, steps[1]);
    Utils::planeIntersection(normals[2], minPt,
                             origin, direction, steps[2]);
    Utils::planeIntersection(normals[3], maxPt,
                             origin, direction, steps[3]);
    Utils::planeIntersection(normals[4], maxPt,
                             origin, direction, steps[4]);
    Utils::planeIntersection(normals[5], maxPt,
                             origin, direction, steps[5]);

    std::function<bool(float)> pointInsideBoundingBox = [&](float step){
        auto point = origin + direction.normalized() * step;
        ROS_INFO_STREAM("pointInsideBoundingBox; step: " << step
                                                         << " intersection: " << point.x() << " " << point.y() << " " << point.z());
        return point.x() >= inputMinPt.x - border && point.y() >= inputMinPt.y - border && point.z() >= inputMinPt.z - border &&
               point.x() <= inputMaxPt.x + border && point.y() <= inputMaxPt.y + border && point.z() <= inputMaxPt.z + border;

    };

    ROS_INFO_STREAM("boundingBoxRayIntersection; exit Intersections: "
                            << steps[0] << " " << steps[1] << " "
                            << steps[2] << " " << steps[3] << " "
                            << steps[4] << " " << steps[5]
    );
    float tMin_ = 1e10, tMax_ = -1e10;
    bool intersected = false;
    for (auto step : steps)
    {
        if (step < tMin_ && pointInsideBoundingBox(step)) // step is smaller than tMin and point lies within bounding box
        {
            intersected = true;
            tMin_ = step;
        }


        if (step > tMax_ && pointInsideBoundingBox(step))  // step is bigger than tMax and point lies within bounding box
        {
            intersected = true;
            tMax_ = step;
        }

    }

    tMin = tMin_;
    tMax = tMax_;

    return intersected;
}

#endif //MEDIASSIST3_RELIABILITY_MODIFIER_OCTREESPHERESEARCH_H
