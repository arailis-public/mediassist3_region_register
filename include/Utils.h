#pragma once

#include <set>

#include <vtkPointSet.h>
#include <vtkSmartPointer.h>

#include <vtkSTLReader.h>
#include <vtkOBJReader.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkPLYReader.h>

#include <vtkUnstructuredGrid.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// This class provides some util functions
// Because these are just helper functions, we don't need a class instance
class Utils {
    
public:
    // Checks if file is pcd and loads it
    static bool checkPCD (
        std::string filename,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud
    );
    
    // Convert a file into an vtk point set
    static vtkSmartPointer<vtkPointSet> convertFile (std::string filename);
    
    // Create a pcl cloud out of a vtk point set
    static void createInputCloud (
        vtkSmartPointer<vtkPointSet> grid,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud
    );
    
    // Manages the creation of the input cloud data and checks if this operation was successful
    static bool loadPointCloud (
        std::string filename,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud
    );
    
    // copy a pointcloud with the corresponding pointindex added to each point
    static void copyPointCloudWithIndex(pcl::PointCloud<pcl::PointXYZRGB>::Ptr src, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr trgt);

    template<class PointT>
    inline static void removeByLabel(typename pcl::PointCloud<PointT>::Ptr src, std::set<unsigned int> labels) {
        for (int i = 0; i<src->size();)
        {
            if (labels.find(src->points.at(i).label) != labels.end())
            {
                // element is already present
                src->points.erase(src->points.begin()+i);
            } else {
                // element is not present
                ++i;
            }
        }
    }

    // remove labeled points from another point cloud
    static void removePointsFromCloudByLabel(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr src, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr remove);

    // Check if a flag for the source filename has been set
    static int checkForFlag(
        int argc, 
        char *argv[],
        std::string flag,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud
    );

    static Eigen::Vector3f projectOntoRay(const Eigen::Vector3f &origin, const Eigen::Vector3f &target, const Eigen::Vector3f &point);

    static double* getResolution(const double* corners, int gridSize);

    static bool planeIntersection(const Eigen::Vector3f& planeNormal, const Eigen::Vector3f& planePoint,
                                  const Eigen::Vector3f& rayOrigin, const Eigen::Vector3f& raydirection,
                                  float& t);

};
