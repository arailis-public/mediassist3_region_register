#include <iostream>
#include <signal.h>

#include <ros/ros.h>

#include <QApplication>
#include <QTimer>
#include <QObject>

#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>
#include <pcl/visualization/point_picking_event.h>

#include "include/GUI/pclviewer.h"
#include "include/IO/InputHandler.h"
#include "include/Utils.h"

#include "main.moc"

using namespace std::chrono_literals;

// Show help if the user made a mistake
void showHelp (const char* program_name)
{
    std::cout << std::endl;
    std::cout << "Usage: rosrun mediassist3_region_register region_register input:=/dense_map/points2" << std::endl;
    std::cout << "The file must be in one of the following formats: PCD, STL, OBJ, VTK, VTU, VTP, PLY" << std::endl;
    std::cout << " " << std::endl;
    std::cout << "Flags:" << std::endl;
    std::cout << "--source:  file/to/source/cloud for the source (preoperative) file cloud" << std::endl;
    std::cout << "--target:  file/to/target/cloud for the target (intraoperative) file cloud" << std::endl;
    std::cout << "-h:  Show this help." << std::endl;
    std::cout << " " << std::endl;
    std::cout << "If no source file is specified, the parameter server is searched automatically." << std::endl;
    std::cout << "If no target file is specified, the program tries to search for a ROS node publishing the file" << std::endl;
}

int main (int argc, char *argv[]) {
    // Initialize ROS
    ros::init (argc, argv, "mediassist3_region_register");
    ros::NodeHandle nh;
    std::string source_filename;
        
    // Help option
    if (pcl::console::find_switch (argc, argv, "-h"))
    {
        showHelp (argv[0]);
        return 0;
    }
    
    // Cloud pointer for the preoperative pcd file
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud (new pcl::PointCloud<pcl::PointXYZRGB>());
    // Try to find a flag for the preoperative cloud
    int src_result = Utils::checkForFlag(argc, argv, "--source", source_cloud);
    // Handle result 
    switch (src_result) {
        // If no flag has been provided, try to search the ROS parameter server for the stl file of the preoperative liver
        case 0:
            ROS_INFO_STREAM("No cloud provided via file input. Searching parameter server for file.");
            if (nh.getParam("preoperative_meshes/liver_surface/filename", source_filename)) {
                // If such a file was found, try to load it into the point cloud
                ROS_INFO_STREAM("Found file. Trying to load...");
                // Display error message if not successful
                if (!Utils::loadPointCloud(source_filename, source_cloud)) {
                    ROS_ERROR_STREAM("Failed to load the parameter server file.");
                    return -1;
                }
                ROS_INFO_STREAM("File loaded successfully.");
            // Abort if no stl file was found
            } else {
                ROS_ERROR_STREAM("Error: Could not find file using the parameter server. Aborting.");
                showHelp (argv[0]);
                return -1;
            }
            break;
        // Error message if flag is there, but loading resulted in an error
        case -1:
            ROS_ERROR_STREAM("Error loading source point cloud. Please make sure the point cloud is in the right format (STL, OBJ, VTK, VTU, VTP, PLY or PCD.");
            showHelp (argv[0]);
            return -1;
        // Default case, erverything worked
        default:
            ROS_INFO_STREAM("Loading of source file successful.");
            break;
    }
    
    // Start the QT application 
    QApplication a (argc, argv);
    
    // Initiate main pcl viewer
    PCLViewerRef m_pclV = std::make_shared<PCLViewer>(source_cloud);
    
    // setup point markings
    m_pclV->getLeftViewer()->registerMouseCallback( &PCLViewer::setSrcCorrespondenceRegion, *m_pclV);
    m_pclV->getRightViewer()->registerMouseCallback( &PCLViewer::setTrgtCorrespondenceRegion, *m_pclV);
    
    // Cloud pointer for the target pcd file
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_cloud (new pcl::PointCloud<pcl::PointXYZRGB>());
    InputHandlerRef m_riH;
    // Try to find a flag for the preoperative cloud
    int trgt_result = Utils::checkForFlag(argc, argv, "--target", target_cloud);
    // Handle result 
    switch (trgt_result) {
        // If no flag was used, it will be done via a ros topic, so create an input handler instance 
        case 0:
            m_riH = std::make_shared<InputHandler>(m_pclV);
            ROS_INFO_STREAM("No target file provided. Searching for ROS publisher " << nh.resolveName("/dense_map/points2"));
            break;
        // Error message if flag is there, but loading resulted in an error
        case -1:
            ROS_ERROR_STREAM("Error loading target point cloud. Please make sure the point cloud is in the right format (STL, OBJ, VTK, VTU, VTP, PLY or PCD.");
            showHelp (argv[0]);
            return -1;
        // Default case, erverything worked
        default:
            m_pclV->updateTargetCloud(target_cloud);
            ROS_INFO_STREAM("Loading of target file successful.");
            break;
    }
    
    // A timer called every 100ms to spin ROS
    QTimer *timer = new QTimer();
    QObject::connect(timer, &QTimer::timeout, ros::spinOnce);
    timer->start(100);
    // Show the viewer
    m_pclV->show ();
    // Allow keyboard interrupts
    signal(SIGINT, SIG_DFL);

    return a.exec ();
}
