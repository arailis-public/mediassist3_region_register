#pragma once

#include <QThread>
#include <QMetaType>
#include <QString>

#include <iostream>

#include "RegionContainer.h"

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/transformation_estimation_svd.h>

// This class does the actual registration, using a new thread
class RegistrationThread : public QThread 
{
    Q_OBJECT
    
public:
    // Constructor
    RegistrationThread(
        int iterations, 
        double error,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud,
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_source_cloud,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_cloud,
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_target_cloud,
        const std::vector<RegionContainerRef> &regContainers
    );
    ~RegistrationThread();
    
    // Main threading function
    void run() override;

signals:
    // Emit the current progress
    void updateProgress(
        int iteration,
        pcl::PointCloud<pcl::PointXYZRGB> t_source_cloud, 
        pcl::PointCloud<pcl::PointXYZRGB> t_regions_src,
        std::vector<RegionContainerRef> c_regContainers
    );

    // Emit that the registration is done
    void regDone( 
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_source_cloud,
        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> transformed_regions_src,
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4 transformation
    );
    
private:
    
    // Perform an initial alignment of the point clouds
    Eigen::Affine3f initialAlignment();
    
    // Define the region indices in the vectors
    void setRegionIndices();
    
    // Set the cloud containing non regions
    void setNonRegionCloud();
    
    // Create a new correspondence
    void createCorrespondences();
    
    // Estimates the error used for the registration
    float estimateError();
    
    // Adjust weighting factor
    void adjustDynamicWeighting(int iteration);
    
    // Class members of existing point clouds, used as copies for registration
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr c_source_cloud;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr c_target_cloud;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr c_indx_target_cloud;
    std::vector<RegionContainerRef> c_regContainers;
    // Clouds used in the registration itself
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr loop_indx_source_cloud;
    // Cloud used to display the regions
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr regions_src;
    
    // Vector storing the transformed source regions
    std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> transformed_regions_src;
    
    // The correspondences stored for the registration
    pcl::CorrespondencesPtr correspondences;
    
    // Vectors containing index information used for the registration
    std::vector<int> source_region_indices;
    std::vector<int> target_region_indices;
    
    // The weighting factor used for the correspondence estimation
    const float weightingCorrespondences = 0.0001;
    
    // Weighting factor used for the error estimation and weighting scheme
    float weightingDynamic = 1000;
    
    // Iteration counter
    int m_iterations;
    // Error value
    double m_error;
    
    // Check if the tree for non region points has been set
    bool treeSet = false;
    
    // The point clouds containing all points not belonging to a region
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_non_region;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr cloud_non_region_labeled;
    // A kd tree for all target points not belonging to a region
    pcl::KdTreeFLANN<pcl::PointXYZRGB> treeNonRegion;
};
