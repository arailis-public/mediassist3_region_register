#pragma once

#include <QtWidgets>

// This class handles the options menu
class OptionsMenu : public QWidget {
    Q_OBJECT
     
public:
    OptionsMenu(QWidget *parent = 0);
    
    int getIterationCount() { return spinBoxIterationCount->value(); }
    double getError() { return spinBoxErrorValue->value(); }
    
    // Sets the options for the spinboxes
    void setSpinBoxes();
    // Create the main options menu
    void createOptionsMenu();
    
private Q_SLOTS:
    // Slots to accept and reject values
    void accepted();
    void rejected();
    
private:    
    // Spinboxes for values
    QSpinBox *spinBoxIterationCount;
    QDoubleSpinBox *spinBoxErrorValue;
    // These variables contain the spinbox values (needed if the options menu is rejected)
    int m_iter;
    double m_error;
}; 
