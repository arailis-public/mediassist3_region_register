//
// Created by martin on 03.03.21.
//

#include "../../include/PCL_Extensions/InteractorHandler.h"

#include <pcl/visualization/interactor_style.h>
#include <vtkCamera.h>
#include <vtkProperty.h>

#include <QDebug>

/**
 * constructor that does some minor setup
 */
InteractorHandler::InteractorHandler() : pcl::visualization::PCLVisualizerInteractorStyle() {
    UseTimersOff(); // we dont use timers -> prevents error logs

    // Setup initial circle
    circle_source->GeneratePolygonOff();
    circle_source->SetNumberOfSides(50);
    circle_source->SetRadius(*circle_radius);
    circle_source->SetCenter(circle_center);
    circle_source->SetNormal(circle_normal);

    circle_mapper->SetInputConnection(circle_source->GetOutputPort());
    circle_actor->SetMapper(circle_mapper);
    circle_actor->GetProperty()->SetOpacity(0.8);
};

/**
 * @brief Emits the events via superclasses "mouse_signal_"
 */
void InteractorHandler::OnLeftButtonDown() {
    // copy form PCLVisualizerInteractorStyle

    // get x/y event pos
    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];

    // set state in case of region select
    if (this->IsRegionSelect()) {
        isSelecting = true;
    }

    // double click?
    if (Interactor->GetRepeatCount () == 0) {
        MouseEvent event (MouseEvent::MouseButtonPress, MouseEvent::LeftButton, x, y, Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey (), IsRegionSelect());
        mouse_signal_ (event);
    } else {
        MouseEvent event (MouseEvent::MouseDblClick, MouseEvent::LeftButton, x, y, Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey (), IsRegionSelect());
        mouse_signal_ (event);
    }
    // no super call --> would enable zooming/...

    triggerCoupledRenderWindows();
}


/**
 * @brief Emits the events via superclasses "mouse_signal_"
 * Stops current state if in regionSelectMode
 */
void InteractorHandler::OnLeftButtonUp() {
    // copy form PCLVisualizerInteractorStyle
    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];
    MouseEvent event (MouseEvent::MouseButtonRelease, MouseEvent::LeftButton, x, y, Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey (), IsRegionSelect());
    mouse_signal_ (event);

    // stop state
    if (IsRegionSelect()) {
        isSelecting = false;
    }

    // no super call --> would enable zooming/...
    triggerCoupledRenderWindows();
}


/**
 * @brief Emits the events via superclasses "mouse_signal_"
 * calls vtkInteractorStyleRubberbandPick OnLeftButtonDown (rotating of pcl)
 */
void InteractorHandler::OnRightButtonDown() {
    // copy form PCLVisualizerInteractorStyle
    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];

    // double click ?
    if (Interactor->GetRepeatCount () == 0) {
        MouseEvent event (MouseEvent::MouseButtonPress, MouseEvent::RightButton, x, y, Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey (), IsRegionSelect());
        mouse_signal_ (event);
    } else {
        MouseEvent event (MouseEvent::MouseDblClick, MouseEvent::RightButton, x, y, Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey (), IsRegionSelect());
        mouse_signal_ (event);
    }
    // enable panning (bound to Left on Default)
    pcl::visualization::PCLVisualizerInteractorStyle::Superclass::Superclass::OnLeftButtonDown();

    triggerCoupledRenderWindows();
}

/**
 * @brief Emits the events via superclasses "mouse_signal_"
 * calls vtkInteractorStyleRubberbandPick OnLeftButtonUp (rotating of pcl)
 */
void InteractorHandler::OnRightButtonUp() {
    // copy form PCLVisualizerInteractorStyle
    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];
    MouseEvent event (MouseEvent::MouseButtonRelease, MouseEvent::RightButton, x, y, Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey (), IsRegionSelect());
    mouse_signal_ (event);

    // enable panning (bound to Left on Default)
    pcl::visualization::PCLVisualizerInteractorStyle::Superclass::Superclass::OnLeftButtonUp();

    triggerCoupledRenderWindows();
}

/**
 * @brief Updates region selection if in RegionSelectionMode
 * calls superclasses OnMouseMove
 */
void InteractorHandler::OnMouseMove() {
    // update circle
    if (IsRegionSelect()) {
        //MoveCircleToFocal();
        CameraNormalToCircleNormal();
    }

    // copy form PCLVisualizerInteractorStyle
    int x = this->Interactor->GetEventPosition()[0];
    int y = this->Interactor->GetEventPosition()[1];
    MouseEvent event (MouseEvent::MouseMove, MouseEvent::NoButton, x, y, Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey (), IsMoveDuringSelect());
    mouse_signal_ (event);

    // enables defaults
    PCLVisualizerInteractorStyle::Superclass::Superclass::OnMouseMove();

    triggerCoupledRenderWindows();
}

/**
 * @brief enlarges circle (if shift is pressed and circle is displayed)
 */
void InteractorHandler::OnMouseWheelBackward() {
    if (IsRegionSelect() && this->Interactor->GetShiftKey() == 1) {
        ResizeCircle(0.1);
        return;
    }
    // enables zoom
    PCLVisualizerInteractorStyle::Superclass::Superclass::OnMouseWheelBackward();

    triggerCoupledRenderWindows();
}

/**
 * @brief shrinks circle (if shift is pressed and circle is displayed)brief
 */
void InteractorHandler::OnMouseWheelForward() {
    if (IsRegionSelect() && this->Interactor->GetShiftKey() == 1) {
        ResizeCircle(-0.1);
        return;
    }
    // enables zoom
    PCLVisualizerInteractorStyle::Superclass::Superclass::OnMouseWheelForward();

    triggerCoupledRenderWindows();
}



/**
 * @brief Starts the Regionselect, Enables circle
 */
void InteractorHandler::StartRegionSelect() {
    this->regionSelect = true;
    if (!circle_setup)
    {
        SetupCircleRenderers();
    }

    SetCircleDisplay(true);
}

/**
 * @brief Stops the Regionselect, Disables circle
 */
void InteractorHandler::StopRegionSelect() {
    this->regionSelect = false;
    this->isSelecting = false;
    SetCircleDisplay(false);
}

/**
 * @brief checks whether a move occured during region select
 * @return bool flag
 */
bool InteractorHandler::IsMoveDuringSelect() {
    return IsRegionSelect() && isSelecting;
}


/**
 * @brief Method to change circle display, adds/removes actors
 * @param isDisplayed flag to disable / enable circle display
 */
void InteractorHandler::SetCircleDisplay(bool isDisplayed) {
    circle_actor->SetVisibility(isDisplayed);
}


/**
 * @brief moves circle to mouse position, same normale as camera normal
 */
void InteractorHandler::MoveCircleToFocal() {
    // get current mouse pos on screen
    auto x = this->Interactor->GetEventPosition()[0];
    auto y = this->Interactor->GetEventPosition()[1];

    // find corresponding world coords
    double* mouse_world_coords = MouseToWorldPoint(x, y);

    // update circle
    SetCenter(mouse_world_coords);
}

void InteractorHandler::CameraNormalToCircleNormal() {
    // get current mouse pos on screen
    auto x = this->Interactor->GetEventPosition()[0];
    auto y = this->Interactor->GetEventPosition()[1];

    // find render / camera that is used
    auto renderer = this->Interactor->FindPokedRenderer((int) x, (int) y);
    auto camera = renderer->GetActiveCamera();

    // circle is always parallel to camera
    camera->GetViewPlaneNormal(circle_normal);

}

/**
 * @brief converts a viewport point to a world point
 * @param mouse_x click x position
 * @param mouse_y click y position
 * @return double* that corresponds to the world point
 */
double* InteractorHandler::MouseToWorldPoint(double mouse_x, double mouse_y) {
    // find renderer / camera the screen coords belong to
    auto renderer = this->Interactor->FindPokedRenderer((int) mouse_x, (int) mouse_y);
    auto camera = renderer->GetActiveCamera();

    // get focal point of camera
    double cameraFP[4];
    camera->GetFocalPoint(cameraFP);

    // convert focal point to display point
    renderer->SetWorldPoint(cameraFP);
    renderer->WorldToDisplay();
    auto displayCoords = renderer->GetDisplayPoint();
    // z of mouse event is set to focal point z on screen
    double mouse_z = displayCoords[2];

    // convert mouse screen x,y,z to world x,y,z
    renderer->SetDisplayPoint(mouse_x, mouse_y, mouse_z);
    renderer->DisplayToWorld();

    // return world point
    return renderer->GetWorldPoint();
}

/**
 * @brief redraws the circle
 */
void InteractorHandler::RedrawCircle() {
    // set normal / center
    circle_source->SetCenter(circle_center);
    circle_source->SetNormal(circle_normal);

    // prevent oversizing of circle
    bool reachedLimit = false;
    if (isSet(circle_min_radius) && *circle_radius < circle_min_radius) {
        *circle_radius = circle_min_radius;
    }
    if (isSet(circle_max_radius) && *circle_radius > circle_max_radius) {
        *circle_radius = circle_max_radius;
    }
    // set radius
    circle_source->SetRadius(*circle_radius);


    // toggle color if circle reached a lower or upper limit
    if (isSet(circle_min_radius) && *circle_radius <= circle_min_radius ||
        isSet(circle_max_radius) && *circle_radius >= circle_max_radius
            ) {
        circle_actor->GetProperty()->SetColor(1,0,0);  // red
    } else {
        circle_actor->GetProperty()->SetColor(1,1,1);  // white
    }

    //rerender
    win_->Render();
}

/**
 * @brief resizes the circle
 * @param factor resize factor
 */
void InteractorHandler::ResizeCircle(double factor) {
    *this->circle_radius *= (1+factor);
    this->RedrawCircle();
    triggerCoupledCircle();
}

/**
 * @brief moves the circle
 * @param coords coordinates to move the circle
 */
void InteractorHandler::SetCenter(double* coords) {
    this->circle_center[0] = coords[0];
    this->circle_center[1] = coords[1];
    this->circle_center[2] = coords[2];
    this->RedrawCircle();
    triggerCoupledCircle();
}

void InteractorHandler::SetNormal(double* normal) {
    this->circle_normal[0] = normal[0];
    this->circle_normal[1] = normal[1];
    this->circle_normal[2] = normal[2];
    this->RedrawCircle();
    triggerCoupledCircle();
}

void InteractorHandler::coupleRenderWindow(InteractorHandler* i) {
    this->coupledInteractors.push_back(i);
}

void InteractorHandler::triggerCoupledRenderWindows() {
    for (auto i : coupledInteractors)
    {
        i->win_->Render();
    }
}

void InteractorHandler::OnKeyDown() {
    if (Interactor->GetKeyCode() == 'X' || Interactor->GetKeyCode() == 'x')
    {
        pcl::visualization::KeyboardEvent event (true, Interactor->GetKeySym (), Interactor->GetKeyCode (), Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey ());
        keyboard_signal_ (event);
        return;
    }
    pcl::visualization::PCLVisualizerInteractorStyle::OnKeyDown();

    triggerCoupledRenderWindows();
}

void InteractorHandler::OnKeyUp() {
    if (Interactor->GetKeyCode() == 'X' || Interactor->GetKeyCode() == 'x')
    {
        pcl::visualization::KeyboardEvent event (false, Interactor->GetKeySym (), Interactor->GetKeyCode (), Interactor->GetAltKey (), Interactor->GetControlKey (), Interactor->GetShiftKey ());
        keyboard_signal_ (event);
        return;
    }
    pcl::visualization::PCLVisualizerInteractorStyle::OnKeyDown();

    triggerCoupledRenderWindows();
}

void InteractorHandler::SetViewportsToDisplayCircle(int *rens_) {
    for (int *ren = rens_; ren < rens_ + sizeof(rens_)/sizeof(*rens_); ++ren)
    {
        viewportsWithCircle.insert(*ren);
    }
}

void InteractorHandler::SetupCircleRenderers()
{
    circle_renderers.clear();

    int i = 0;
    vtkRenderer* renderer;
    this->rens_->InitTraversal();
    // vector to hold pointers to already existing renderers
    std::vector<vtkRenderer*> crens;
    while((renderer = rens_->GetNextItem()) != nullptr)
    {
        if (viewportsWithCircle.empty())
        {
            // no special renderers, do for all
            auto circle_renderer = circle_renderers.insert(vtkSmartPointer<vtkRenderer>::New()).first;
            (*circle_renderer)->SetViewport(renderer->GetViewport());
            (*circle_renderer)->SetActiveCamera(renderer->GetActiveCamera());
            (*circle_renderer)->SetLayer(win_->GetNumberOfLayers()); // top most renderer
            (*circle_renderer)->AddActor(circle_actor);
        }
        else if (viewportsWithCircle.find(i) != viewportsWithCircle.end())
        {
            // only renderers that are specified
            auto circle_renderer = circle_renderers.insert(vtkSmartPointer<vtkRenderer>::New()).first;
            (*circle_renderer)->SetViewport(renderer->GetViewport());
            (*circle_renderer)->SetActiveCamera(renderer->GetActiveCamera());
            (*circle_renderer)->SetLayer(win_->GetNumberOfLayers()); // top most renderer
            (*circle_renderer)->AddActor(circle_actor);
        }
        crens.push_back(renderer);
        ++i;

    }

    // in the next step we clear renderers and readd them after we've added our circle renderes
    // this is because the last added renderer determines the clipping range of the corresponding camera
    // both (initial/cirlce) renderers use same camera,
    // readding the initial renderers fixes clipping issues
    win_->GetRenderers()->RemoveAllItems();

    for (auto& renderer: circle_renderers)
    {
        win_->AddRenderer(renderer);
    }

    for (auto renderer: crens)
    {
        win_->AddRenderer(renderer);
    }

    win_->SetNumberOfLayers(win_->GetNumberOfLayers()+1);
    circle_setup = true;
}

void InteractorHandler::triggerCoupledCircle() {
    for (auto* i : coupledCircleRadius)
    {
        i->RedrawCircle();
    }
}
