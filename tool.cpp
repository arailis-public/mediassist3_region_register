#include <iostream>
#include <string>

#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>

#include "include/RegistrationThread.h"
#include "include/RegionContainer.h"
#include "include/Utils.h"

#include "tool.moc"

using namespace std::chrono_literals;

// Show help if the user made a mistake
void showHelp (const char* program_name) {
    std::cout << std::endl;
    std::cout << "Usage: rosrun mediassist3_region_register tool src_cloud.pcd trgt_cloud.pcd iterationCount errorValue src_reg0.pcd trgt_reg0.pcd src_reg1.pcd trgt_reg1.pcd..." << std::endl;
    std::cout << "All point cloud files must be in pcd-format." << std::endl;
    std::cout << "Multiple regions can be entered. In this case, for every source region a target region must be entered." << std::endl;
    std::cout << "iterationCount should be an integer, errorValue should be a float value." << std::endl;
    std::cout << " " << std::endl;
    std::cout << "-h:  Show this help." << std::endl;
}

// Source and target cloud and regions
pcl::PointCloud<pcl::PointXYZRGB>::Ptr src_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
pcl::PointCloud<pcl::PointXYZRGB>::Ptr trgt_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());

// Indexed source and target cloud
pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_src_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_trgt_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());

/**
 * @brief Called when registration is finished
 * @param t_preoperative_cloud - Transformed source cloud
 * @param transformed_regions_src Vector containing transformed source regions
 * @param transformation Final transformation
 */
void handleRegDone(
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_source_cloud,
            std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> transformed_regions_src,
            pcl::registration::TransformationEstimationSVD<pcl::PointXYZRGB,pcl::PointXYZRGB>::Matrix4 transformation
        ) {
        // Save the transformed data
        std::cout << "Saving data..." << std::endl; 
        pcl::io::savePCDFileASCII("src_cloud_transformed.pcd", *t_source_cloud);
        // Create a transformation file and store it
        std::ofstream file("transformation.txt");
        if (file.is_open()) {
            file << transformation << '\n';
            file.close(); 
        }
        std::cout << "Saving complete." << std::endl;
}

int main (int argc, char *argv[]) {
        
    // Help option
    if (pcl::console::find_switch (argc, argv, "-h") || argc % 2 == 0) {
        showHelp (argv[0]);
        return 0;
    }
    
    // Vector for reg containers
    std::vector<RegionContainerRef> regContainers;
    // iteration count and error value used for registration boundaries
    int     iterationCount;
    float   errorValue;
    
    // Try to load the source and target cloud files
    if (pcl::io::loadPCDFile<pcl::PointXYZRGB> (argv[1], *src_cloud) == -1 || 
        pcl::io::loadPCDFile<pcl::PointXYZRGB> (argv[2], *trgt_cloud) == -1) {
        PCL_ERROR ("Couldn't read source and/or target cloud file.\n");
        return (-1);
    }
    
    // Convert command line arguments to int and float
    std::string argIC   = argv[3];
    std::string argErr  = argv[4];
    std::size_t posIC, posErr;
    iterationCount = std::stoi(argIC, &posIC);
    errorValue = std::stof(argErr, &posErr);
    
    // Set the indexed clouds
    Utils::copyPointCloudWithIndex(src_cloud, indx_src_cloud);
    Utils::copyPointCloudWithIndex(trgt_cloud, indx_trgt_cloud);
    
    // Now we are loading the regions
    // Because the command line arguments for the regions start at position 5, we start with this and iterate in 2 steps for src and trgt region
    for(int i = 5; i < argc; i += 2) {
        // Create a region container instance and store it
        RegionContainerRef regionContainer = std::make_shared<RegionContainer>();
        regContainers.push_back(regionContainer);
        // Point cloud instances
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_src_region(new pcl::PointCloud<pcl::PointXYZRGBL>());
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr indx_trgt_region(new pcl::PointCloud<pcl::PointXYZRGBL>());
        
        // Try to load the source and target region files
        if (pcl::io::loadPCDFile<pcl::PointXYZRGBL> (argv[i], *indx_src_region) == -1 || 
            pcl::io::loadPCDFile<pcl::PointXYZRGBL> (argv[i + 1], *indx_trgt_region) == -1) {
            PCL_ERROR ("Couldn't read one or multiple region files.\n");
            return (-1);
        }
        
        // Set the region container data
        regContainers.back()->addPointsToRegion(indx_src_region, true);
        regContainers.back()->addPointsToRegion(indx_trgt_region, false);
        regContainers.back()->setUnlabeledRegions();
    }
    
    // Registration thread
    RegistrationThread *t = new RegistrationThread (
                                iterationCount, errorValue, 
                                src_cloud, indx_src_cloud, 
                                trgt_cloud, indx_trgt_cloud, 
                                regContainers
                           );
    // Connect the deletion and registration handling
    QObject::connect(t, &RegistrationThread::finished, t, &QObject::deleteLater);
    QObject::connect(t, &RegistrationThread::regDone, &handleRegDone);
    
    // Begin registration
    std::cout << "Starting registration." << std::endl;
    t->start();
    // Wait until the thread is finished
    while(!t->isFinished()) {
    }
    
    return 0;
}
