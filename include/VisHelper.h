#pragma once

#include <pcl/visualization/pcl_visualizer.h>
#include "RegionContainer.h"

// This class handles some of the visualization during runtime
// The VisHelper is not needed as another instance, so only static functions are used
class VisHelper {

public:
    
    // Display registered regions and the registered source cloud in the bottom viewport 
    static void visualizeRegistration(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_source_cloud,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr t_regions_src,
        const std::vector<RegionContainerRef> &regContainers,
        pcl::visualization::PCLVisualizer::Ptr viewer
        );

    // Clean up some pointclouds after a registration
    static void cleanUp(
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr highl_source_cloud,
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr highl_target_cloud,
            pcl::visualization::PCLVisualizer::Ptr viewer_left,
            pcl::visualization::PCLVisualizer::Ptr viewer_right
        );

    // Update a point cloud
    template <class PointT>
    static void updateCloud(
        pcl::visualization::PCLVisualizer::Ptr viewer,
        typename pcl::PointCloud<PointT>::Ptr cloud,
        pcl::visualization::PointCloudColorHandlerCustom<PointT> color,
        std::string id,
        int size
        );

    // Adds cloud to a viewer, checks if this cloud has a set color
    static void checkColorAndAddCloud(
        pcl::visualization::PCLVisualizer::Ptr viewer,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
        std::string id
        );
};

/**
 * @brief Updates a point cloud with a color
 * @param viewer to display the updated cloud
 * @param cloud to be updated
 * @param color of the points
 * @param id of the cloud
 * @param size of the points
 */
template<class PointT>
void VisHelper::updateCloud(
        pcl::visualization::PCLVisualizer::Ptr viewer,
        typename pcl::PointCloud<PointT>::Ptr cloud,
        pcl::visualization::PointCloudColorHandlerCustom<PointT> color,
        std::string id,
        int size
        ) {
    // Remove the cloud and add it back to the viewer
    // @note Currently (PCL 1.10.) there are some issues inside the updatePointCloud function
    // If the cloud contains three or more points and points are deleted until only two points per cloud remain,
    // a length_error in vector::reserve will be thrown out if another deletion occurs
    // Removing and adding the cloud back provide a workaround for this issue
    // This issue has been fixed in PCL 1.11, but this won't be released on Ubuntu LTS until 22.04
    if (viewer->contains(id)) {
        viewer->removePointCloud(id);
    }

    viewer->addPointCloud (cloud, color, id);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, size, id);
}
